package main

import (
	"errors"
	"fmt"
	"math/rand"
	"sync"
	"time"
)

var (
	RequestEmptyEnd int // 只能在 request() 中使用，模拟某个请求后没有数据的结束下标

	ErrRequestError = errors.New("request error")
	ErrRequestEmpty = errors.New("request empty")
)

func init() {
	rand.Seed(time.Now().UnixNano())
	RequestEmptyEnd = rand.Intn(20)
}

// TODO: 改造 main() 和 request() 代码，提升并发性能
// TODO: 不知道请求多少次，需要对并发数量进行控制（Goroutine Pool：协程池）
// TODO: request() 出错或没有数据后停止后续请求，但已经请求并正常处理返回的数据需要保存到 result 中
func main() {
	threads := 5

	queue := make(chan int, threads+1)
	resultQ := make(chan int, threads*2)
	flag := make(chan bool, threads)

	result := make([]int, 0)
	wg := sync.WaitGroup{}

	for i := 0; i < threads; i++ {
		wg.Add(1)
		go func(q chan int, c chan int, f chan bool) {
			defer wg.Done()
		LOOP:
			for {
				select {
				case <-f:
					f <- true
					break LOOP
				case m := <-q:
					r, err := request3(m)
					if err != nil {
						f <- true
						break LOOP
					}
					c <- r
				}
			}
		}(queue, resultQ, flag)
	}

	go func(p chan int, f chan bool) {
	LOOP:
		for i := 0; ; i++ {
			select {
			case <-f:
				f <- true
				break LOOP
			case p <- i:
			}
		}
	}(queue, flag)

	go func(rs *[]int, c chan int) {
		for r := range c {
			*rs = append(*rs, r)
		}
	}(&result, resultQ)

	wg.Wait()
	fmt.Println(result)
}

// 模拟 http 请求，返回时间不一
// 可能出错（90% 概率模拟出错）
// 可能某个请求后没有数据（只能逐条请求后才能发现之后是否还有数据）
func request3(i int) (int, error) {
	r := rand.Intn(4)
	time.Sleep(time.Duration(r) * time.Second)
	// 模拟 error
	if rand.Intn(100) > 90 {
		fmt.Printf("error: %d\n", i)
		return r, ErrRequestError
	}
	// 模拟 empty
	if i >= RequestEmptyEnd {
		fmt.Printf("empty: %d\n", i)
		return r, ErrRequestEmpty
	}
	fmt.Printf("proc %d, result: %d\n", i, r)
	return r, nil
}
