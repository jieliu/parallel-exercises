package main

import (
	"fmt"
	"math/rand"
	"time"
)

func init() {
	rand.Seed(time.Now().UnixNano())
}

// TODO: 改造 main() 代码，提升并发性能
// TODO: N 较大时，需要对并发数量进行控制（Goroutine Pool：协程池）
func main() {
	N := 100
	poolSize := 5

	pool := make(chan int, poolSize+1)
	ch := make(chan int)

	for i := 0; i < poolSize; i++ {
		go request2(i, pool, ch)
	}

	go func(p chan int) {
		for i := 0; i < N; i++ {
			p <- i
		}
		close(p)
	}(pool)

	result := make([]int, 0)

	for i := 0; i < N; i++ {
		result = append(result, <-ch)
	}

	fmt.Println(result)
}

// 模拟 http 请求，返回时间不一
func request2(i int, pool chan int, ch chan int) {
	for j := range pool {
		r := rand.Intn(4)
		time.Sleep(time.Duration(r) * time.Second)
		fmt.Printf("proc %d:%d, result: %d\n", i, j, r)
		ch <- r
	}
}
