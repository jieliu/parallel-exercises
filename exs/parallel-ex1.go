package main

import (
	"fmt"
	"math/rand"
	"time"
)

func init() {
	rand.Seed(time.Now().UnixNano())
}

// TODO: 改造 main() 代码，提升并发性能
func main() {
	N := 5
	result := make([]int, 0)
	ch := make(chan int)
	for i := 0; i < N; i++ {
		go request1(i, ch)
	}
	for i := 0; i < N; i++ {
		result = append(result, <-ch)
	}

	fmt.Println(result)
}

// 模拟 http 请求，返回时间不一
func request1(i int, ch chan int) {
	r := rand.Intn(4)
	time.Sleep(time.Duration(r) * time.Second)
	fmt.Printf("proc %d, result: %d\n", i, r)
	ch <- r
}
